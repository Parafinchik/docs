# Release and pre environments

The release and pre environments are automatically updated when a Release Manager
tags the monthly release candidate and the final release respectively.

When release candidates are tagged, e.g.:

```
/chatops run release tag 14.0.0-rc44
```

An Omnibus pipeline on dev is triggered as a result to build the package,
`Ubuntu 20.04-upload-deploy` is the job that triggers the deploy to the [pre environment]:

* Pipeline example: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/200819
* Job example[^1]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/10357540

```bash
Ready to send trigger for environment(s): pre
Triggering pipeline https://ops.gitlab.net/api/v4/projects/151/trigger/pipeline for ref master, status code: 201 Created
Deployer build triggered at https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/654716 on master for the pre environment
```

In the same vein, when tagging the final version of the release, e.g.:

```
/chatops run release tag 14.0.0
```

An Omnibus pipeline on dev is triggered as a result to build the package,
`Ubuntu 20.04-upload-deploy` is the job that triggers the deploy to the [release environment]:

* Pipeline example: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipelines/204981
* Job example: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/10603257

```bash
14.0.0+ee.0
Ready to send trigger for environment(s): release
Triggering pipeline https://ops.gitlab.net/api/v4/projects/151/trigger/pipeline for ref master, status code: 201 Created
Deployer build triggered at https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/715343 on master for the release environment
```

## Manual deployments

Sometimes we want to deploy a newer version to pre outside of a monthly release
for testing purposes. It is safe to do so via the usual chatops command, and
will not cause issues with the next RC deployment, e.g.:

```
/chatops run deploy 14.2.202108020321-b94d2478afd.c278429ee94 --pre
```

[^1]: Job example is `Ubuntu 16.04-upload-deploy` newer versions of the pipeline should use `Ubuntu 20.04-upload-deploy` to trigger the deploy to pre.

[pre environment]: https://pre.gitlab.com/help
[release environment]: https://release.gitlab.net/help
