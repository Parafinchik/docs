## How to fix stale deployments?

The `/chatops run rollback check <env>` command relies on the status of the deployments
associated with the environment to determine the package to rollback to. These deployment
entries are handled by our tooling, particularly by the `DeploymentTracker` class, and are
updated and created during the actual deployment performed by the coordinated pipeline.

On some occasions, some of these entries are stalled, meaning they will keep the
`running` status indefinitely:

![gprd deployment stale](./images/deployment_gprd_stale.png)

This leads to the `rollback check` command returning inconsistent responses:
the command will return the rollback is not possible because there's an ongoing 
deployment, referring to the `5771` deployment that has been "running" for over a month.

![rollback unavailable](./images/rollback_unavailable.png)

Ending a deployment is an option that is not available through the UI. One way to fix
this inconsistency is to modify the data in production:

1. First, we need to get the ID's associated with the environment that we intend
  to modify, gprd in this case, particularly we need:
   * The project ID: Visibile on the [GitLab Security] repository.
   * Environment ID: Visible by navigating to the specific entry on the [environments page].
2. Using a rails console, update the environment to use the `failed` status.
   * Replace `project_id` and `environment_id` with the ID's found in the first step.
   * `status: 1` refers to the `running` status.
   * `status: 3` refers to the `failed` status.
   * `iid: [5771]` includes the ID's of the deployment that needs to be updated.

```ruby
[ gprd ] production> Deployment.where(project_id: 123, environment_id: 456, status: 1, iid: [5771]).update(status: 3)
=> [#<Deployment id: 227030558, iid: 5771, project_id: 123, environment_id: 456, ref: "14-6-auto-deploy-2021121603", tag: false, sha: "3c2328d924643c7a407a0631e08ab6fb907d8c92", user_id: 123, deployable_type: nil, created_at: "2021-12-16 12:01:40.456259000 +0000", updated_at: "2022-02-11 16:23:14.568541445 +0000", on_stop: nil, finished_at: nil, status: "failed", cluster_id: nil, deployable_id: nil, archived: false>]
```

3. Refresh the environment page, deployment should no longer be `running`:

![gprd deployment failed](./images/gprd_environment_failed.png)

Note that this process is rather intrusive since it requires a rails console production, it might
be okay to use it when a quick solution is needed, but in the long term we should [embedd this process]
into our tooling so Release Managers can update the deployments without using a gprd rails console.

[`DeploymentTracker` class]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/release_tools/deployments/deployment_tracker.rb
[GitLab Security]: https://gitlab.com/gitlab-org/security/gitlab
[environments page]: https://gitlab.com/gitlab-org/security/gitlab/-/environments
[deployments API]: https://docs.gitlab.com/ee/api/deployments.html#update-a-deployment
[embedd this process]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2249
