# Overview

This document describes the procedure to rollback a GitLab.com deployment by
re-deploying a previous package. Rollbacks can help recover from incidents
caused by code changes.

[[_TOC_]]

## Rolling back

### 1. Gather Package information

A rollback should not be attempted with a deployment containing post-deployment
migrations as they can leave the database in an unknown state. Determining if
it's safe to rollback is currently determined by a ChatOps command, but a
manual procedure is described below.

To determine if it's safe to rollback, run the following ChatOps command:

```
/chatops run rollback check <ENV>
```

Where `ENV` is either:

* `gstg` - Staging
* `gprd` - Production

<details>
  <summary>Example Commands</summary>

To check Production:

```
/chatops run rollback check gprd
```

To check Staging:

```
/chatops run rollback check gstg
```
</details>

The output shows the following information:

* If it thinks it is safe to roll back
* If a deployment is in progress
* The number and type of migrations included in the last package
* The name and link to the new, current, and previous deployed commits and a comparison link.
* The actual roll back command that should be run

The package name we want to roll back to is the "previous" package name
displayed in the output from the `rollback check` command above. Copy it to pass
to the `deploy` command later in this procedure.

:warning: **If a deploy is considered in progress, the command will incorrectly
state which package to roll back too.  You'll want to use the "current" package
name, and use the manual procedure noted below.** :warning:

<details>
  <summary>Manual steps</summary>

Replace `SHA` with the SHA of the deployment to roll back to (985b57c4ca9 in the
above example) and open the following link in your browser:

```
https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/tags?utf8=%E2%9C%93&search=SHA
```

You should see a single tag with a name like so:

```
13.9.202102091820+985b57c4ca9.7ad6df8e35c
```

It's possible for more than one tag to be displayed. In that case, you should
pick the most recent tag based on the tagging date (`202102091820` in the above
example).

To obtain the package version, simply replace the `+` with a `-`, like so:

```
13.9.202102091820-985b57c4ca9.7ad6df8e35c
```
</details>

### 2. Notify the EOC

Notify `@sre-on-call` in `#production` that a rollback is about to be started.
If the rollback is going to be performed on Production make sure they know that
Canary will also be drained. 

### 3. Perform the rollback

Production rollbacks require us to first drain Canary:

```
/chatops run canary --disable --production
```

Initiate a rollback using chatops. Using the package name
information found in step 1 above, to roll back (without the production flag,
deployer will operate against staging):

```
/chatops run deploy <PACKAGE NAME> --rollback [--production]
```

An alternative [manual procedure](#manual-rollback) is described below in this
document.

<details>
  <summary>Example Commands</summary>

Roll back Staging to a previous package:

```
/chatops run deploy 13.9.202102091820-985b57c4ca9.7ad6df8e35c --rollback
```

Roll back Production to a previous package:

```
/chatops run deploy 13.9.202102091820-985b57c4ca9.7ad6df8e35c --rollback --production
```
</details>

### 4. Monitor the `<environment>-prepare` CI Job

During the rollback monitor the `<environment>-prepare` CI job for a potential
failure. If it becomes stuck in a loop while waiting for the Ominibus lock,
perform the following steps:

1. Re-verify that no other deployment job is in progress
    * If a new deployment is in progress, evaluate the situation to determine if it
      should be cancelled, or if it is safe to move forward.
1. If no new deployment is in progress, unlock the environment via ChatOps:

   ```
   /chatops run deploy unlock <ENVIRONMENT>
   ```

### 5. (Optional) - gitaly and praefect

By default the rollback pipeline **will not downgrade gitaly and praefect**.

If a rollback is desired, a final stage with manual jobs is provided as part of the
rollback pipeline. This optional stage can run as soon as the rest of the web-fleet stage is completed. Always rollback Praefect before Gitaly.

### 6. Ensure consistency between Staging and Production

With the [introduction of multiple staging environments][staging], Staging and
Production versions are meant to be kept in sync. If we rollback one, we should
ensure the other has a matching package deployed. This may involve canceling an
ongoing deployment to the other environment, or rolling it back to the matching
version.

[staging]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/608

## After a rollback

Once the rollback pipeline completes an "<env> finished a rollback to <package>" message will be posted in the [#announcements Slack channel](https://gitlab.slack.com/archives/C8PKBH3M5). You can check the rollback's success by running 
   ```
   /chatops run auto_deploy status
   ```
The [release management dashboard](https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1&refresh=5m) will also update. Note, it may take several minutes for the new version to be displayed. 

Following a rollback the cause of the problem will still need to be fixed. The exact process of this may differ based on the
reason for the rollback, but in most cases this will involve asking the
appropriate developers to open a merge request to revert or fix the problem.

**The rollback is now complete**

## (Optional) - In case of ChatOps failure

In case of chatops failure follow the manual steps to run a rollback.

### Manually checking for migrations

#### 1. Pick the environment

First, open the environment page of the environment you wish to roll back:

* [staging](https://gitlab.com/gitlab-org/security/gitlab/-/environments/1516167)
* [canary](https://gitlab.com/gitlab-org/security/gitlab/-/environments/1913972)
* [production](https://gitlab.com/gitlab-org/security/gitlab/-/environments/1700351)

#### 2. Determine the deploy to roll back to

Next, determine what deploy you want to roll back. In almost (if not all) cases
this will simply be the latest deploy recorded. With that deploy noted down,
pick the deploy that came before it. This will be the deploy to roll back to.

Example: the current deploy is deploy #1991, which deployed commit 9c71509c. The
previous deploy is #1986 which deployed commit 8ce1730f.

#### 3. Compare the changes to see if there are migrations

Open the following link in your browser:

```
https://gitlab.com/gitlab-org/security/gitlab/-/compare/PREVIOUS...LATEST
```

Replace `PREVIOUS` with the SHA of the deploy to roll back to (8ce1730f in the
above example), and replace `LATEST` with the SHA of the current deployment
(9c71509c).

With the link open, look for any newly added files in the `db/post_migrate`
directory. If no new files are added, it's safe to roll back.

### Manual rollback

To create a rollback pipeline without ChatOps available, create a new pipeline on the `deployer` project with the following variables:

| key | value | example |
| --- | ----- | ------- |
| `DEPLOY_ROLLBACK` | `true` | N/A |
| `DEPLOY_ENVIRONMENT` | Environment name | `gprd` |
| `DEPLOY_VERSION` | Package string | `14.7.202201200320-bbf52b48f4d.3b4ebcefa97` |
